<?php
//  L'identifiant API doit être créé sur oMailgw-UI
$config['api_user']='xxxxxxxxxxxxxxxxxxxx'; 
$config['api_passwrd']='xxxxxxxxxxxxxxxxxxxx'; 
//  Où se trouve l'API d'oMailgw-Ui 
$config['api_url']='https://omailgw.retzo.net/api';
// Le nombre de donnée maximum 
$config['api_max_data']=40;
// Attente minimum entre 2 envois (évite les 429 Too Many...)
$config['api_sleep_after_send']=1;

// Nombre de tentative après erreur HTTP 
$config['curl_nb_tentative']=5;
// Temps entre 2 tentatives
$config['curl_sleep_tentative']=60; 

// CURL option https://www.php.net/manual/en/function.curl-setopt.php
$config['curl_opt'] = [
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 15,
	CURLOPT_CONNECTTIMEOUT => 15,
	CURLOPT_SSL_VERIFYPEER => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
];
$config['curl_opt_httpheader'] = [
	"Accept: application/json",
	"Content-Type: application/json",
	"User-Agent: oMailgw-cli (https://framagit.org/omailgw/omailgw-cli)"
];
// var directory for omailgw-cli
$config['var_dir']='/opt/omailgw-cli/var/';
// mail log par défaut (fichier ou "stdin")
$config['default_maillog']='/var/log/mail.log';
// Nom du serveur (hostname)
$config['server_hostname']=preg_replace("# {2,}#","",preg_replace("#(\r\n|\n\r|\n|\r)#","",file_get_contents('/etc/hostname')));
// Année en cours
$config['log_year']=date('Y');
// Chemin de la base sqlite (false = pas de vérification de checksum avant envoi -> plus de requête sur le serveur)
$config['sqlite_file']=$config['var_dir'].'database.sql';
// Supprimer les données checksum de la base, valeur strtotime: https://www.php.net/manual/fr/function.strtotime.php
$config['sqlite_clean_data']='-3 day';

// Path pour postmap (postfix tools for transport & blacklist file)
$config['path_postmap']="/usr/sbin/postmap";
// Chemin vers la commande mailq
$config['path_mailq']='/usr/bin/mailq';
// Répertoire de lock
$config['lock_dir']='/var/lock';
// id du lock (nom du fichier)
$config['lock_id']='omailgwcli';

// Regex des lignes du logs à récupérer
// Pour vous aider : https://regex101.com/
$config['log_regex'] = array(
	// Seulement si authentification
	'/^(?<queueId>[A-Z0-9]+): client=(?<smtpd_client>[a-z0-9\-.\[\]]+), sasl_method=(?<smtpd_method>[A-Z0-9-]+), sasl_username=(?<smtpd_username>\S+)$/',
	// Sans authentification
	'/^(?<queueId>[A-Z0-9]+): client=(?<smtpd_client>[a-z0-9\-.\[\]]+)$/',
	// Local
	'/^(?<queueId>[A-Z0-9]+): uid=(?<uid>[0-9]+) from=<(?<from>.+)>/',
	//
	'/^(?<queueId>[A-Z0-9]+): (?<cleanup>.+) from (?<smtpd_client>[a-z0-9\-.\[\]]+): (?<msg>.+); from=<(?<from>.+)> to=<(?<to>.+)> proto=ESMTP/',
	'/^(?<queueId>[A-Z0-9]+): message-id=<(?<message_id>\S+)>/',
	'/^(?<queueId>NOQUEUE): reject: RCPT from (?<smtpd_client>.+): (?<smtpd_codeError>[0-9\s\.]+) (?<msg>.+); from=<(?<from>\S+)> to=<(?<to>\S+)> proto=ESMTP helo=<(?<smtpd_helo>\S+)>$/',
	'/^(?<queueId>[A-Z0-9]+): from=<(?<from>\S+)?>, size=(?<qmgr_size>\S+), nrcpt=(?<qmgr_nrcpt>\S+) \((?<qmgr_msg>.+)\)/',
	'/^(?<smtp_queueId>[A-Z0-9]+): to=<(?<smtp_to>\S+)>, relay=(?<smtp_relay>\S+), delay=(?<smtp_delay>\S+), delays=(?<smtp_delays>\S+), dsn=(?<smtp_dsn>\S+), status=(?<smtp_status>\S+) \((?<smtp_msg>.+)\)/',
	'/^(?<smtp_queueId>[A-Z0-9]+): to=<(?<smtp_to>\S+)>, orig_to=(?<smtp_orig_to>\S+), relay=(?<smtp_relay>\S+), delay=(?<smtp_delay>\S+), delays=(?<smtp_delays>\S+), dsn=(?<smtp_dsn>\S+), status=(?<smtp_status>\S+) \((?<smtp_msg>.+)\)/',
);


// A exclure des logs : 
$config['log_regex_exclude'] = array(
	'/Relay access denied;/',
);

// Rspamd add symbol detail 
// Request by GET API 
$config['rspamd_check']=false;
$config['rspamd_cache']=$config['var_dir']."rspamd_history";
$config['rspamd_password']="*****";
$config['rspamd_url']="http://localhost:11334/history?Password=".$config['rspamd_password'];

# Expiration des "remember"
$config['expire'] = [
	'token' => 86400,
	'smtp-auth' => 86400,
	'blacklist' => 86400,
	'transport' => 86400,
];
# Demande durant le "helo" 
$config['api_demand'] = ['smtp-auth', 'blacklist', 'transport'];

// Assurez vous que $config['api_demand'] contienne "smtp-auth"
// Add Postfix : smtpd_recipient_restrictions = permit_sasl_authenticated, permit_mynetworks, reject_unauth_destination
// Password file for smtpd-auth
$config['smtpd_auth_passwd_file']=$config['var_dir']."smtpd_auth_passwd_file";
// Commande de test pour valider le fichier de configuration smtpd-auth
// null pour ne pas tester
$config['smtpd_auth_tester']="echo '<password>' | pamtester -v smtp <username> authenticate";

// Assurez vous que $config['api_demand'] contienne "blacklist"
// Add Postfix : 
//	- smtpd_recipient_restrictions = check_recipient_access hash:/etc/postfix/recipient_access
//	- smtpd_sender_restrictions = check_sender_access hash:/etc/postfix/sender_access
// Blacklist files (sender/recipient
$config['blacklist_sender_file']=$config['var_dir']."blacklist_sender_file";
$config['blacklist_recipient_file']=$config['var_dir']."blacklist_recipient_file";

// Assurez vous que $config['api_demand'] contienne "transport"
// Add Postfix : 
//	- transport_maps = hash:/etc/postfix/transport, regexp:/etc/postfix/transport.regexp
$config['transport_file']=$config['var_dir']."transport";
$config['transport_regex_file']=$config['var_dir']."transport.regexp";

// Commande pour reload postfix
// Doit être exécutable avec l'utilisateur qui lance omailgw-cli.php, si c'est omailgw, préciser ceci dans le sudo : 
//	 echo "omailgw   ALL=NOPASSWD:/etc/init.d/postfix reload" > /etc/sudoers.d/50-omailgw
$config['sudo_postfix_reload']="/usr/bin/sudo /etc/init.d/postfix reload";

?>
