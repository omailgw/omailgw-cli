# oMailgw-cli

Permet la communication avec l'API [oMailgw-API](https://framagit.org/omailgw/omailgw-api) d'une passerelle e-mail sortante sous postfix. Ce client : 

- Parse les logs Postfix et envoie les données à oMailgw-API via HTTP Complète  avec les données de Rspamd (symboles) les logs pour les e-mails rejetés  en SPAM
- Transmet le contenu du "spooler" (e-mail en file d'attente)
- Récupère :
  - Les authentifications SMTP
  - Les blackliste (options)
  - Les fichiers de transports postfix (routage e-mail) (options)

Les commandes de l'API utilisé par oMailgw-cli sont les suivantes : https://app.swaggerhub.com/apis/retzo.net/oMailgw/#/cli

## Installation

###  Pré-requis 

* Avoir une passerelle e-mail sortante sous postfix
* Avoir [oMailgw-API](https://framagit.org/omailgw/omailgw-api) d'installé sur le même ou sur un autre serveur (une seule instance de l'API suffit à plusieurs "cli")
* Avoir configuré un utilisateur avec le rôle "mailgw" : 

```bash
curl -X POST \
  'https://votreapi/api/admin/user/add' \
  --header 'Accept: application/json' \
  --header 'Authorization: Bearer VOTRE_TOKEN_AUTHENTIFICATION_ROOT' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "email": "mailgw1@retzo.net",
  "password": "*******************",
  "role": "mailgw",
  "rapport": 0
}'
```

* Avoir configuré un "serveur", ici `mailgw.example.org` :


```bash
curl -X 'POST' \
  'https://votreapi/api/server/add?hostname=mailgw&domain=example.org' \
  --header "Authorization: Bearer VOTRE_TOKEN_AUTHENTIFICATION_ROOT" \
  --header 'accept: application/json' \
```

### Installation

Dépendance : 

```bash
apt install php-cli php-curl php-sqlite3 php-pear git
```

Installation

```bash
# Création d'un utilisateur pour le lancement du omailgw-cli
# Doit être dans le groupe "adm" pour lire les log sous Debian
useradd -s /bin/false -m -d /opt/omailgw-cli/ omailgw
# Pour pouvoir lire les fichiers logs sous debian
usermod omailgw -G adm 
cd /opt
git clone https://framagit.org/omailgw/omailgw-cli.git
cd omailgw-cli
mkdir var
chown omailgw /opt/omailgw-cli -R
cp config-default.php config.php
```

Éditer le fichier config.php, au minimum : 

```php
//  L'identifiant API doit être créé sur oMailgw-UI
$config['api_user']='mailgw1@retzo.net'; 
$config['api_passwrd']='*******************'; 
//  Où se trouve l'API
$config['api_url']='https://votreapi/api';
```

### SMTP auth passwod file

Dans un premier temps on met en place libpam-pwdfile (sur ce modèle : https://seenthis.net/messages/522996) : 

```bash
# Installation libpam-pwdfile
apt-get install libpam-pwdfile pamtester
# Ajout d'un compte test
echo "test@retzo.net:"$( mkpasswd -m sha-512 "Az15S4f56rxx12v4h9a45d" ) > /etc/postfix/omailgw_passwd_file 
# Configuration pam pour SMTP
echo 'auth            required        pam_pwdfile.so pwdfile=/etc/postfix/smtpd_auth_passwd_file
account         required        pam_permit.so
session         required        pam_permit.so
password        required        pam_deny.so' > /etc/pam.d/smtp
# Test 
echo "Az15S4f56rxx12v4h9a45d" | pamtester -v smtp test@retzo.net authenticate
# Dépendance pour postfix
apt install libsasl2-modules postfix sasl2-bin
# Ajout pour SMTPD
echo "pwcheck_method: saslauthd
mech_list: plain login" > /etc/postfix/sasl/smtpd.conf
# Config  saslauthd
echo 'START=yes
DESC="SASL Authentication Daemon"
NAME="saslauthd-postf"
MECHANISMS="pam"
MECH_OPTIONS=""
THREADS=5
#OPTIONS="-c -m /var/run/saslauthd"
OPTIONS="-c -r -m /var/spool/postfix/var/run/saslauthd"
' > /etc/default/saslauthd-postfix
# Pour éviter un petit bug
dpkg-statoverride --add root sasl 710 /var/spool/postfix/var/run/saslauthd
adduser postfix sasl
# Application des changements
service saslauthd  restart
# Test auth : 
testsaslauthd -u "test@retzo.net" -p "Az15S4f56rxx12v4h9a45d" -f /var/spool/postfix/var/run/saslauthd/mux -s smtp
# Ajout de l'authentification dans la configuration postfix
postconf -e 'smtpd_sasl_auth_enable = yes'
postconf -e 'smtpd_sasl_security_options = noanonymous'
postconf -e 'smtpd_recipient_restrictions = permit_sasl_authenticated, permit_mynetworks, reject_unauth_destination'
# Application de la config postfix
service postfix restart
```

Le fichier renseigné par `$config['smtpd_auth_passwd_file']` est généré automatiquement (écrasé) par retour de l'API mais vous pouvez définir des authentifications "static" en créant un fichier `$config['smtpd_auth_passwd_file'].static` qui sera inclus dans le fichier généré.

Vous pouvez aussi appliquer un post traitement, juste avant l'écriture du fichier `$config['smtpd_auth_passwd_file']` en créant un fichier `custom-smtp_auth.php` à la racine, il est lu et exécuté dans la fonction (voir `custom-smtp_auth.php_example`)

Si vous utilisez l'authentification SMTP (c.f. TutorielMailgw.md):

```bash
# Init
touch /opt/omailgw-cli/var/smtpd_auth_passwd_file 
chown omailgw /opt/omailgw-cli/var -R
# Lien
ln /opt/omailgw-cli/var/smtpd_auth_passwd_file /etc/postfix/smtpd_auth_passwd_file 
```

### 	Blackliste

Le comportement avec les fichier *.static est le même que pour les authentifications SMTP

```bash
# Init
touch /opt/omailgw-cli/var/blacklist_sender_file
touch /opt/omailgw-cli/var/blacklist_recipient_file
chown omailgw /opt/omailgw-cli/var -R
postmap /opt/omailgw-cli/var/blacklist_sender_file
postmap /opt/omailgw-cli/var/blacklist_recipient_file
# Lien
ln -s /opt/omailgw-cli/var/blacklist_* /etc/postfix/
```

Dans le fichier /etc/postfix/main.cf

```
# Blacklist from
smtpd_sender_restrictions = check_sender_access hash:/etc/postfix/blacklist_sender_file
# Blacklist to
smtpd_recipient_restrictions = check_recipient_access hash:/etc/postfix/blacklist_recipient_file
```

Si jamais vous utilisez l’authentification SMTP + les blacklistes il faut préférer un smtpd_recipient_restrictions :

```
# Blacklist to + authentification SMTP
smtpd_recipient_restrictions = check_recipient_access hash:/etc/postfix/blacklist_recipient_file, permit_sasl_authenticated, permit_mynetworks, reject_unauth_destination
```

### Transport

Ici il y  un comportement pour les *static un peut différent. A admettant une config comme tel : 

* `$config['transport_file']` = /var/omailgw-cli/var/transport
* `$config['transport_regex_file']` = /var/omailgw-cli/var/transport_regex 

Il est possible de créer : 

*  /var/omailgw-cli/var/transport.static.header : qui sera inclus en tête du fichier `$config['transport_file']`
*  /var/omailgw-cli/var/transport.static.footer : qui sera inclus en fin du fichier `$config['transport_file']`
*  /var/omailgw-cli/var/transport_regex.static.header : qui sera inclus en tête du fichier `$config['transport_regex_file']`
*  /var/omailgw-cli/var/transport_regex.static.footer : qui sera inclus en fin du fichier `$config['transport_regex_file']`

Ceci car postfix lis les transport map dans l'ordre et s'arrête sur le premier match. Donc si vous acceptez que les règles statics soit supplanté par les règles indiqué dans omailgw, mettez les dans le static.footer, sinon dans le static.header


```bash
# Init
touch /opt/omailgw-cli/var/transport
touch /opt/omailgw-cli/var/transport.regex
chown omailgw /opt/omailgw-cli/var -R
postmap /opt/omailgw-cli/var/transport
# Lien
ln -s /opt/omailgw-cli/var/transport* /etc/postfix/
```

Dans le fichier /etc/postfix/main.cf

```
transport_maps = hash:/etc/postfix/transport, regexp:/etc/postfix/transport.regexp
```

Ajouter la possibilité pour l'utilisateur "omailgw" de reload postfix par la commande sudo  sans mot de passe : 

```bash
echo "omailgw   ALL=NOPASSWD:/etc/init.d/postfix reload" > /etc/sudoers.d/50-omailgw
```

## Envoi des données

### Manuel 

Pour le lancer et commencer à envoyer des données : 

```bash
sudo -u omailgw php omailgw-cli.php -v --log=/var/log/mail.log
# Ou 
sudo -u omailgw cat /var/log/mail.log | php omailgw-cli.php -v --log=stdin
```

Attention, un checksum des données est effectué et conservé (3 jours par défaut : `$config['sqlite_clean_data']`) il permet d'éviter le nouvel envoi de donnée déjà envoyé (limite la charge server). Pour éviter ce comportement préférer : 
```php
$config['sqlite_clean_data']=null
```

### Tâche planifiée

<u>Conseil</u> tiliser logtail avec le script `cron.sh`  permet de limiter les ressources serveurs aux seules nouvelles informations du log : 

```bash
apt install logtail
```

Éditer éventuellement le script cron.sh avec le path du dossier omailgw-cli (variable `OMAILGW_PATH`)

Installer en tâche planifiée pour l'utilisateur "omailgw" à la fréquence à laquelle vous voulez voir les informations se rafraîchir : 

```bash
crontab -u omailgw -e
```

```bash
# oMailgw cli toutes les 5 minutes
*/5 * * * * bash /opt/omailgw-cli/cron.sh
```

Installer dans logrotate pour ne rien louper : /etc/logrotate.d/rsyslog

```
/var/log/mail.info
/var/log/mail.warn
/var/log/mail.err
/var/log/mail.log
{
        rotate 365		# Rétention légal de 365j
        daily
        missingok
        notifempty
        compress
        delaycompress
        sharedscripts
        prerotate
                sudo -u omailgw bash /opt/omailgw-cli/cron.sh
        endscript
        postrotate
                /usr/lib/rsyslog/rsyslog-rotate
        endscript
}
```

## Intégrer l'historique

Lancer ce bout de script pour intégrer l'historique de 2023 : 

```bash
year=2023
echo > /tmp/parse-history-log-file-error.log
for i in $(find /var/log/ -name mail.log\* -newermt "$year-01-01 01:00:00"); do 
    /usr/bin/php /opt/omailgw-cli/omailgw-cli.php --log=${i} --year=${year}
    if (($?)); then
        echo $i >> /tmp/parse-history-log-file-error.log
        sleep 60
    fi
done
```

De mon côté avec des logs quotidiens qui pèsent en moyenne ~100k (~4000 lignes) pour analyser 6 mois de log ça prend une nuit... :-/

## Usage sans `mailq`
Il est possible d'utiliser oMailgw-cli sur un serveur qui ne dispose pas d'installation complète de postfix (par exemple un serveur qui centralise des logs, ou une machine hôte qui fait tourner un Docker dans lequel il y a postfix qui tourne). Pour ignorer le fait que postfix n'est pas installé (et donc que la commande `mailq` n'existe pas), il est possible de modifier la configuration suivante dans `config.php` : 

```php
$config['path_mailq']='cat /dev/null';
```

## E-mail entrant

La passerelle e-mail peut aussi recevoir des e-mails en entrant, ce cas n'est pas géré par omailgw, il est *peut être* possible de les ignorer  dans le config.php avec les regex (`$config['log_regex_exclude']`) en suivant les noms de domaines de réception ou de ne faire un parse que sur le serveur entrant choisie (`$config['log_regex']`)

## Auteurs

* [David Mercereau](https://david.mercereau.info/) [@kepon](https://framagit.org/kepon)

## Licence

Licence GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

Voir Licence.txt
