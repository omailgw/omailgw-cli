<?php

######################
# Gestion des options / vérifications
######################

# Gestion des options du script
# https://www.php.net/manual/fr/function.getopt.php
$shortopts  = "";
$shortopts .= "v::"; // verbose
$shortopts .= "Y::"; // Years of log
$shortopts .= "c::"; // config file
$shortopts .= "h"; // Help
$shortopts .= "d"; // Debug
$longopts  = array(
    "log::",    // Valeur optionnelle pour le log (chemin ou stdin)
	"config::",	// Précision du fichier de configuration
    "year::",	// Valeur optionnelle pour l'année du log
	"debug",	// Affichage du Json
    "help"	// Valeur optionnelle pour l'année du log
);
$options = getopt($shortopts, $longopts);

# Include default config
include('config-default.php');
# Include du fichier de config
$config_file='config.php';
if (isset($options['config'])) {
	$config_file=$options['config'];
}
include($config_file);
# Include des fonctions
include('inc-function.php');

# Affichage du "help"
if (isset($options['h'])) {
	echo "php parse.php [--log=][--config=][-v][-h]\n";
	echo "	-v : verbose\n";
	echo "	--log=mail.log : log file path or 'stdin' for stdin input \n";
	echo "	--config=config-.php : config file path \n";
	echo "	-h : this help\n";
	exit();
}

// Gestion du lock pour éviter de lancer plusieurs fois le script
$lock = new ExclusiveLock($config['lock_id']);
if( $lock->lock( ) === FALSE ) {
	echococo('Le programme est déjà lancé');
    exit(12);
}

# Gestion de l'option "log"
$maillog=$config['default_maillog'];
if (isset($options['log'])) {
	$maillog=$options['log'];
}
echococo('Utilisation du fichier de log '.$maillog);
# Lecture du stdin si celui-ci est solicité
function stdin_stream() {
    while ($line = fgets(STDIN)) {
        yield $line;
    }
}
# Option d'année :
if (isset($options['year'])) {
	$config['log_year']=$options['year'];
}
echococo('Année du log '.$config['log_year']);

# Test de lecture du log
if ($maillog != 'stdin' && !is_readable($maillog)) {
	echococo('Erreur to read '.$maillog);
	exit('Erreur to read '.$maillog);
}

# Gestion des fichier gz
if ($maillog == 'stdin') {
	$lines = stdin_stream();
	echococo('Ouverture du STDIN');
} elseif (pathinfo($maillog, PATHINFO_EXTENSION) == 'gz') {
	$lines = gzfile($maillog);
	echococo('Ouverture en Gzip');
} else {
	$lines = file($maillog);
}

######################
# Database
######################
if ($config['sqlite_file'] !== null) {
	// Connect DB
	try {
		$dbco = new PDO("sqlite:".$config['sqlite_file']);
		$dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch ( PDOException $e ) {
		die("Connexion à la base ".$config['sqlite_file']." a échoué : ".$e->getMessage());
	}
	// Create DB if not exists
	try {
		$create = $dbco->query("CREATE TABLE IF NOT EXISTS checksum (
			id INTEGER PRIMARY KEY,
			checksum INTEGER NOT NULL,
			date INTEGER NOT NULL
		);");
		$create = $dbco->query("CREATE TABLE IF NOT EXISTS remember (
			key VARCHAR PRIMARY KEY,
			content VARCHAR NOT NULL,
			date INTEGER NOT NULL
		);");
	} catch ( PDOException $e ) {
		echo 'Error initializing tables '.$config['sqlite_file'];
		echococo($e->getMessage());
		die();
	}
} else {
	$dbco = new PDO("sqlite:/dev/null");
}

######################
# Programme
######################
# Authentification
$get_remember_token = get_remember('token');
if (empty($get_remember_token['date'])) {
	echococo('Enregistrement du premier token');
	$token=auth2api();
	put_remember('token', $token);
} elseif ($get_remember_token['date']+$config['expire']['token'] < time())  {
	echococo('Token expired');
	$token=auth2api();
	put_remember('token', $token);
} else {
	$token=$get_remember_token['content'];
}
echococo('Token d\'authentification : '.$token);

# Hello API
# For smtp-auth
$hello = hello2api($token, $config['api_demand']);

if ($config['rspamd_check'] === true) {
	get_rspamd_data();
}

# Parse du fichier de log
$data = array();
$dataChecksum = array();
$countData=0;
foreach ($lines as $line_num => $line) {

	//echococo("\nTraitement de la ligne : ".$line);

	// Première découpe
	$preg_match_global = preg_match('/^(?<date>([a-zA-Z]+[\s]+[0-9]+[\s]+[0-9:]+)|([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9:\.]+[+-][0-9:]+))[\s]+(?<host>\S+)[\s]+(?<pid>\S+):[\s]+(?<msg>.+)/', $line, $line_matches);
	if (!$preg_match_global) {
		continue;
	}

	// Récupération du champ date
	if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}T/', $line_matches['date'])) {
		$date_parts = explode('T', $line_matches['date']);
		$date_iso = explode('-', $date_parts[0]);
		$date_iso[0] = $config['log_year'];
		$global_data['date'] = strtotime(implode('-', $date_iso) . 'T' . $date_parts[1]);
	} else {
		$date_parts = explode(' ', $line_matches['date']);
		$global_data['date'] = strtotime($date_parts[1] . ' ' . $date_parts[0] . ' ' . $config['log_year'] . ' ' . $date_parts[2]);
	}
	
	// Récupération du champs server
	$global_data['server'] = $config['server_hostname'];
	// Récupération du champs process
	$process = preg_split("/[\/\[\]]/", $line_matches['pid']);
	$global_data['process'] = $process[1];
	// Test de la ligne sur les regex
	foreach	($config['log_regex'] as $log_regex) {
		$preg_match=preg_match($log_regex, $line_matches['msg'], $msg_matches);
		if($preg_match == 1) {
			# Exclusion de la config log_regex_exclude
			foreach	($config['log_regex_exclude'] as $log_regex_exclude) {
				$preg_match_exclude=preg_match($log_regex_exclude, $line_matches['msg'], $msg_matches_exclude);
				if($preg_match_exclude == 1) {
					echococo('Regex FOUND '.$log_regex.' BUT exclude by '.$log_regex_exclude."\n>>>> ".$line);
				} else {
					echococo('Regex FOUND '.$log_regex."\n>>>> ".$line);
					if ($global_data['process'] == "smtpd") {
						$msg_matches['smtpd_date']=$global_data['date'];
						// S'il n'y a pas de queueId de défini on en génère un
						if ($msg_matches['queueId'] == 'NOQUEUE') {
							$msg_matches['queueId'] = genQueueId();
							//~ var_dump($msg_matches);
							//~ exit();
						}
					} elseif ($global_data['process'] == "smtp" || $global_data['process'] == "lmtp" || $global_data['process'] == "error") {
						$msg_matches['smtp_date']=$global_data['date'];
					} else {
						$msg_matches['smtpd_date']=$global_data['date'];
					}
					$msg_matches['server']=$global_data['server'];
					$msg_matches['process']=$global_data['process'];
					// Si j'ai activer la base, je vérifie si le checksum n'a pas déjà été envoyé...
					if ($config['sqlite_file'] !== null) {
						$checksum=crc32(json_encode($config['log_year'].'-'.$line));
						if ($dbco->query("SELECT COUNT(*) FROM checksum WHERE checksum = '".$checksum."'")->fetchColumn() == 0) {
							echococo("! Checksum = add data\n");
							# Suppression des numéro de key, garder juste le snom
							# https://codereview.stackexchange.com/questions/104817/clean-regex-matches-with-named-matches
							foreach ($msg_matches as $key => $value) {
								if (is_int($key))
									unset($msg_matches[$key]);
							}
							// Check rspamd
							if ($config['rspamd_check'] === true && $msg_matches['process'] == 'cleanup' && isset($msg_matches['cleanup']) && preg_match("/^milter-reject:/", $msg_matches['cleanup'])) {
								$parse_rspamd_data_return = parse_rspamd_data($msg_matches['queueId'], $data);
								if ($parse_rspamd_data_return != false && $parse_rspamd_data_return != '') {
									$msg_matches['msg'] = $parse_rspamd_data_return;
								}
							}
							array_push($data, $msg_matches);
							array_push($dataChecksum, ['date'=>$global_data['date'], 'checksum'=>$checksum]);
							$countData++;
						} else {
							echococo("Checksum présent != add data\n");
						}
					} else {
						# Suppression des numéro de key, garder juste le snom
						# https://codereview.stackexchange.com/questions/104817/clean-regex-matches-with-named-matches
						foreach ($msg_matches as $key => $value) {
							if (is_int($key))
								unset($msg_matches[$key]);
						}
						// Check rspamd
						if ($config['rspamd_check'] === true && $msg_matches['process'] == 'cleanup' && preg_match("/^milter-reject:/", $msg_matches['cleanup'])) {
							$parse_rspamd_data_return = parse_rspamd_data($msg_matches['queueId'], $data);
							if ($parse_rspamd_data_return != false) {
								$msg_matches['msg'] = $parse_rspamd_data_return;
							}
						}
						// Ajout sans vérification
						array_push($data, $msg_matches);
						$countData++;
					}
					
				}
			}
		} 
	}
	# Quand on attend le nombre de donnée maximum défini on envoi 
	if ($countData >= $config['api_max_data']) {
		sendLog2apiWithRetry($token, $data, $dataChecksum);
		sleep($config['api_sleep_after_send']);
		$data = array();
		$dataChecksum = array();
		$countData=0;
	}
}
if ($countData != 0) {
	echococo("Envoi final !");
	sendLog2apiWithRetry($token, $data, $dataChecksum);
}

if ($config['sqlite_file'] !== null) {
	cleanOldDataInDb();
}

// Suppression du lock
$lock->unlock();

?>
