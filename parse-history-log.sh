#!/bin/bash

# Rechercher les fichiers après une date donnée
year=2023
yearBefor=$(($year+1)) 
findResult=$(find /var/log/ -name mail.log\* ! -newermt "$yearBefor-01-01 01:00:00" -newermt "$year-01-01 01:00:00" )
echo > /tmp/parse-history-log-file-error.log
nbFindResult=$(echo $findResult | tr ' ' '\n'  | wc -l)
count=0
for i in $findResult; do 
    count=$(expr $count + 1)
    echo "$count/$nbFindResult > $i"
    /usr/bin/php /opt/omailgw-cli/omailgw-cli.php --log=${i} --year=${year}
    if (($?)); then
	echo $i >> /tmp/parse-history-log-file-error.log
	sleep 60
    fi
done

