#!/bin/bash

# Pour récupérer le passif de logtail si jamais il y a eu des erreurs (et donc ne rien louper)
OMAILGW_PATH="/opt/omailgw-cli"

cp ${OMAILGW_PATH}/var/logtail.mail.log.offset ${OMAILGW_PATH}/var/logtail.mail.log.offset.bkp

/usr/sbin/logtail -o ${OMAILGW_PATH}/var/logtail.mail.log.offset -f /var/log/mail.log | php ${OMAILGW_PATH}/omailgw-cli.php --log=stdin $@ &>/tmp/omailgw-cli.log
er=$?
if (( ${er} == 12 )) 
then
    # Silencieusement... On remet l'offset (le cli est déjà lancé)
    mv ${OMAILGW_PATH}/var/logtail.mail.log.offset.bkp ${OMAILGW_PATH}/var/logtail.mail.log.offset
elif (( ${er} != 0 )) 
then
    echo "Erreur, je récupère le offset sauvegardé et j'affiche le détail. "
    cat /tmp/omailgw-cli.log
    mv ${OMAILGW_PATH}/var/logtail.mail.log.offset.bkp ${OMAILGW_PATH}/var/logtail.mail.log.offset
fi
