<?php

#use PDO;

/**
 * Echo if verbose mode
 *
 * @return echo
 */
function echococo($msg)
{
	global $options;
	if (isset($options['v'])) {
		echo $msg . "\n";
	}
}

// Remember in database 
function put_remember($key, $content)
{
	global $config;
	global $dbco;
	if ($config['sqlite_file'] !== null) {
		echococo("Remember put : " . $key . "=" . $content);
		$time = time();
		try {
			$insertcmd = $dbco->prepare("REPLACE INTO remember (key, content, date) 
											VALUES (:key, :content, :date)");
			$insertcmd->bindParam('key', $key, PDO::PARAM_STR);
			$insertcmd->bindParam('content', $content, PDO::PARAM_STR);
			$insertcmd->bindParam('date', $time, PDO::PARAM_INT);
			$insertcmd->execute();
		} catch (PDOException $e) {
			echo "DB error :  " . $e->getMessage();
			die();
		}
	}
}
// Get remember from database 
function get_remember($key)
{
	global $config;
	global $dbco;
	if ($config['sqlite_file'] !== null) {
		echococo("Remember get : " . $key);
		try {
			$selectcmd = $dbco->prepare("SELECT content, date FROM remember
												WHERE key = :key");
			$selectcmd->bindParam('key', $key, PDO::PARAM_STR);
			$selectcmd->execute();
			return $selectcmd->fetch();
		} catch (PDOException $e) {
			echo "DB error :  " . $e->getMessage();
			die();
		}
	} else {
		return null;
	}
}


/**
 * Authentification auprès de l'API
 *
 * @return string
 */
function auth2api()
{
	global $config;
	$apiLogin = $config['api_url'] . "/user/login";
	echococo('Authentification sur l\'API ' . $apiLogin);
	try {
		$curl = curl_init();
		$curlopt = [
			CURLOPT_URL => $apiLogin,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n  \"email\": \"" . $config['api_user'] . "\",\n  \"password\": \"" . $config['api_passwrd'] . "\"\n}",
			CURLOPT_HTTPHEADER => $config['curl_opt_httpheader'],
		];
		curl_setopt_array($curl, $config['curl_opt'] + $curlopt);
		$result = curl_exec($curl);
		$curl_error = curl_error($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response = json_decode($result, true);
		if ($http_code != intval(201)) {
			echococo("Code HTTP : " . $http_code . " Erreur : " . json_encode($response));
			echococo("Reponse : " . json_encode($response));
			echococo("curl_error : " . $curl_error);
			$response = false;
			exit(10);
		} elseif (empty($response['token'])) {
			echo "l'API donne une erreur ";
			echo "Code HTTP : " . $http_code . " Erreur : " . json_encode($response);
			echo "Reponse : " . json_encode($response);
			echo "curl_error : " . $curl_error;
			exit(253);
		}
	} catch (\Throwable $th) {
		throw $th;
	} finally {
		curl_close($curl);
	}
	return $response['token'];
}

/**
 * Communication sur la config du serveur l'API
 *
 * @param string $token
 * @param string $data
 * @return object
 */
function hello2api($token, $demands)
{
	global $config;
	// Default return
	$return = true;
	// Api URL build
	$apiLogin = $config['api_url'] . "/cli/" . $config['server_hostname'] . "?";
	foreach ($demands as $demand) {
		$get_remember[$demand] = get_remember($demand);
		if (empty($get_remember[$demand]['date'])) {
			echococo('Première demande ' . $demand);
			$apiLogin .= "demand[]=" . $demand."&";
			put_remember($demand, 'demand');
		} elseif ($get_remember[$demand]['date'] + $config['expire'][$demand] < time()) {
			echococo('Epiration de la demand ' . $demand);
			$apiLogin .= "demand[]=" . $demand."&";
			put_remember($demand, 'demand');
		} else {
			$apiLogin .= "demand[]=" . $demand . "-change&";
		}
	}
	echococo('Hello sur l\'API ' . $apiLogin);
	// Recup mailq
	$dataJsonEncode = json_encode( [ 'spool' => mailqParse() ]) ;
	//var_dump($dataJsonEncode);
	//exit();
	// Prepar curl
	try {
		$curl = curl_init();
		$curl_opt_httpheader = [
			'Authorization: Bearer ' . $token,
		];
		$curlopt = [
			CURLOPT_URL => $apiLogin,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $dataJsonEncode,
			CURLOPT_HTTPHEADER => array_merge($curl_opt_httpheader, $config['curl_opt_httpheader']),
		];
		curl_setopt_array($curl, $config['curl_opt'] + $curlopt);
		$result = curl_exec($curl);
		$response = json_decode($result, true);
		$curl_error = curl_error($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		// Traitement curl
		if ($http_code == intval(404)) {
			echo "Le server ".$config['server_hostname']." n'a pas été trouvé...\n";
			echo "Demander à un utilisateur 'root' de l'ajouter\n";
			exit(1);
		} elseif ($http_code != intval(200)) {
			echo "l'API donne une erreur \n";
			echo "Code HTTP : " . $http_code . " Erreur : " . json_encode($response)." \n";
			echo "Reponse : " . json_encode($response)." \n";
			echo "curl_error : " . $curl_error." \n";
			exit(254);
		} else {
			if (isset($response['smtp-auth'])) {
				if (count($response['smtp-auth']) != 0) {
					put_smtp_auth_file($response['smtp-auth']);
				}
			}
			if (isset($response['blacklist'])) {
				if (count($response['blacklist']) != 0) {
					put_blacklist_file($response['blacklist']);
				}
			}
			if (isset($response['transport'])) {
				if (count($response['transport']) != 0) {
					put_transport_file($response['transport']);
				}
			}
		}
	} catch (\Throwable $th) {
		throw $th;
	} finally {
		curl_close($curl);
	}
	return $return;
}

/**
 * Envoi des données de log vers l'API
 *
 * @param string $token
 * @param string $data
 * @return object
 */
function sendLog2api($token, $data)
{
	global $config;
	$apiLogin = $config['api_url'] . "/log";
	echococo('Envoi des données sur l\'API ' . $apiLogin);
	$dataJsonEncode = json_encode($data);
	//~ echo($dataJsonEncode); exit();
	# https://stackoverflow.com/a/15643608
	try {
		$curl = curl_init();
		$curl_opt_httpheader = [
			'Authorization: Bearer ' . $token,
		];
		$curlopt = [
			CURLOPT_URL => $apiLogin,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $dataJsonEncode,
			CURLOPT_HTTPHEADER => array_merge($curl_opt_httpheader, $config['curl_opt_httpheader']),
		];
		curl_setopt_array($curl, $config['curl_opt'] + $curlopt);
		$result = curl_exec($curl);
		$response = json_decode($result, true);
		$curl_error = curl_error($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($http_code != intval(200)) {
			echococo("Code HTTP : " . $http_code . " Erreur : " . json_encode($response));
			echococo("Reponse : " . json_encode($response));
			echococo("curl_error : " . $curl_error);
			$response = false;
		} else {
			if (isset($response['data_ignored']) && $response['data_ignored'] != 0) {
				echo ("Attention : data_ignored : " . $response['data_ignored']."\n");
			}
		}
	} catch (\Throwable $th) {
		throw $th;
	} finally {
		curl_close($curl);
	}
	return $response;
}
/**
 * Gestion des envois multiple en cas d'erreur HTTP
 *
 * @param string $token
 * @param string $data
 * @return object
 */
function sendLog2apiWithRetry($token, $data, $dataChecksum)
{
	global $config;
	global $dbco;
	//var_dump(json_encode($data)); exit(12);	
	for ($i = 1; $i <= $config['curl_nb_tentative']; $i++) {
		if (sendLog2api($token, $data) == false) {
			echococo("Tentative $i vers l'API");
			sleep($config['curl_sleep_tentative']);
		} else {
			// Ajout les checksum des données envoyés en DB si activé
			if ($config['sqlite_file'] !== null) {
				echococo("Enregistrement des checksum");
				foreach ($dataChecksum as $dataChecksumParse) {
					try {
						$insertcmd = $dbco->prepare("INSERT INTO checksum (date, checksum) 
														VALUES (:date, :checksum)");
						$insertcmd->bindParam('date', $dataChecksumParse['date'], PDO::PARAM_STR);
						$insertcmd->bindParam('checksum', $dataChecksumParse['checksum'], PDO::PARAM_INT);
						$insertcmd->execute();
					} catch (PDOException $e) {
						echo "DB error :  " . $e->getMessage();
						die();
					}
				}
			}
			break;
		}
		if ($i == $config['curl_nb_tentative']) {
			echo "Trop de tentative vers l'API qui ne répond pas\n";
			exit(254);
		}
	}
}



function cleanOldDataInDb()
{
	global $dbco;
	global $config;
	try {
		$deletecmd = $dbco->prepare("DELETE FROM checksum WHERE date < :date");
		$deletecmd->bindParam('date', $config['sqlite_clean_data'], PDO::PARAM_INT);
		$deletecmd->execute();
	} catch (PDOException $e) {
		echo "DB error : " . $e->getMessage();
		die();
	}
}



/**
 * Simple example PHP code to parse "mailq" output. Useful if you want to show
 * the mail queue status through some web service.
 *
 * Notice that you'll need a mail server (sendmail or, better, postfix) to be
 * running on the same machine as your webserver (often that's less that
 * optimal.)
 *
 * If this doesn't work, be sure to check that you have sufficient permissions
 * to run mailq succesfully as your webserver user.
 * 
 * - Giulio Iotti <giulio@pilv.it>
 * https://gist.github.com/dullgiulio/885906
 */
function mailqParse()
{
	# Récuprération du path de mailq
	global $config;
	$results = array();
	$current_object = null;
	# Ouverture du stream mailq
	$pipe = popen($config['path_mailq'], 'r');
	# Lecture du contenu
	while ($pipe) {
		$line = fgets($pipe);
		if ($line === false)
			break;
		// First and last summary lines are the only ones beginning with a dash.
		if (strncmp($line, '-', 1) === 0)
			continue;
		$line = trim($line);
		// Append the current object to the objects list. No last-object problem,
		// as we certainly have one empty line before the end.
		if (!$line) {
			if ($current_object) {
				$results[] = $current_object;
				$current_object = null;
			}
		}
		// This could be much better so easily (eg: insert months, remove duplications, etc.)
		$res = preg_match('/^([A-Z0-9]{1,20})\s+([1-9][0-9]*)\s+((Mon|Tue|Wed|Thu|Fri|Sat|Sun)\s+[A-Za-z]{3}\s+[0-9]{1,2}\s+[0-9]{2}\:[0-9]{2}\:[0-9]{2})\s+([^ ]+)\s*$/', $line, $matches);
		if ($res) {
			// Insert message ID and other data into the object.
			// It will be competed afterwards.
			$current_object = array(
				'queueId' => $matches[1],
				'size' => intval($matches[2]),
				'date' => strtotime($matches[3]),
				'from' => $matches[5],
				'msg' => '',
				'to' => ''
			);
		} else {
			// Parse error lines and recipients in lines following the message ID.
			if ($current_object) {
				if (strncmp($line, '(', 1) === 0) {
					$current_object['msg'] = substr($line, 6, strlen($line) - 7);
				} else {
					$current_object['to'] .= trim($line);
				}
			}
		}
	}
	pclose($pipe);
	return ($results);
}

// Gestion lock
// https://stackoverflow.com/questions/325806/best-way-to-obtain-a-lock-in-php
class ExclusiveLock
{

	protected $key   = null;  //user given value
	protected $file  = null;  //resource to lock
	protected $own   = FALSE; //have we locked resource

	function __construct($key)
	{
		global $config;
		$this->key = $key;
		//create a new resource or get exisitng with same key
		$this->file = fopen($config['lock_dir'] . "/" . $key . ".lock", 'w+');
	}


	function __destruct()
	{
		if ($this->own == TRUE)
			$this->unlock();
	}

	function lock()
	{
		if (!flock($this->file, LOCK_EX | LOCK_NB)) { //failed
			$key = $this->key;
			error_log("ExclusiveLock::acquire_lock FAILED to acquire lock [$key]");
			return FALSE;
		}
		ftruncate($this->file, 0); // truncate file
		//write something to just help debugging
		fwrite($this->file, "Locked\n");
		fflush($this->file);

		$this->own = TRUE;
		return TRUE; // success
	}


	function unlock()
	{
		$key = $this->key;
		if ($this->own == TRUE) {
			if (!flock($this->file, LOCK_UN)) { //failed
				error_log("ExclusiveLock::lock FAILED to release lock [$key]");
				return FALSE;
			}
			ftruncate($this->file, 0); // truncate file
			//write something to just help debugging
			fwrite($this->file, "Unlocked\n");
			fflush($this->file);
			$this->own = FALSE;
		} else {
			error_log("ExclusiveLock::unlock called on [$key] but its not acquired by caller");
		}
		return TRUE; // success
	}
};

// Générer une QueueId (quand c'est une NOQUEUE ex NOQCDA004A
function genQueueId() {
	$caracteres = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$longueurMax = 7;
	$chaineAleatoire = '';
	for ($i = 0; $i < $longueurMax; $i++) {
		$chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
	}
	return "NOQ".$chaineAleatoire;
}

// For replace mkpasswd command
// https://stackoverflow.com/a/72121914
function cryptPassword($password, $salt = "", $rounds = 20000)
{
	if ($salt == "") {
		// Generate random salt
		$salt = substr(bin2hex(openssl_random_pseudo_bytes(16)), 0, 16);
	}
	// $6$ specifies SHA512
	$hash = crypt($password, sprintf('$6$rounds=%d$%s$', $rounds, $salt));
	return $hash;
}

// Pour enregistrer la config blacklist reçu :
function put_blacklist_file($datas) {
	global $config;
	//var_dump($datas);
	$file_sender_data = '';
	$file_recipient_data = '';
	// Vérification de la présence du fichier "static"
	if (is_file($config['blacklist_sender_file'].".static")) {
		echococo("Un fichier ".$config['blacklist_sender_file'].".static est détecté, il sera inclus");
		$file_sender_data .= "# OMAILGW-CLI - DON'T TOUCH THIS DYNAMIC FILE ! \n";
		$file_sender_data .= "# CREATE SIMILAIRE SAME NAME FILE WITH .static EXTENTION \n";
		$file_sender_data .= file_get_contents($config['blacklist_sender_file'].".static")."\n";
		$file_sender_data .= "# / OMAILGW-CLI \n";
	}
	if (is_file($config['blacklist_recipient_file'].".static")) {
		echococo("Un fichier ".$config['blacklist_recipient_file'].".static est détecté, il sera inclus");
		$file_recipient_data .= "# OMAILGW-CLI - DON'T TOUCH THIS DYNAMIC FILE ! \n";
		$file_recipient_data .= "# CREATE SIMILAIRE SAME NAME FILE WITH .static EXTENTION \n";
		$file_recipient_data .= file_get_contents($config['blacklist_recipient_file'].".static")."\n";
		$file_recipient_data .= "# / OMAILGW-CLI \n";
	}
	// Construction du fichier blacklist_file
	foreach ($datas as $data) {
		if ($data['type'] == 2) {
			if ($data['rules'] != null) {
				$file_recipient_data .= $data['email'] . "	550 Recipient Blacklisted by rules ".$data['rules'];
			} elseif ($data['user_id'] != null) {
				$file_recipient_data .= $data['email'] . "	550 Recipient Blacklisted by ".$data['user_id'];
			} else {
				$file_recipient_data .= $data['email'] . "	550 Recipient Blacklisted";
			}
			$file_recipient_data .= "\n";
		} elseif ($data['type'] == 1) {
			if ($data['rules'] != null) {
				$file_sender_data .= $data['email'] . "	550 Sender Blacklisted by rules ".$data['rules'];
			} elseif ($data['user_id'] != null) {
				$file_sender_data .= $data['email'] . "	550 Sender Blacklisted by ".$data['user_id'];
			} else {
				$file_sender_data .= $data['email'] . "	550 Sender Blacklisted";
			}
			$file_sender_data .= "\n";
		}
	}
	if (is_file('custom-blacklist.php')) {
		echococo("Include custom-blacklist.php");
		include('custom-blacklist.php');
	}
	// Ecriture
	echococo("Ecriture du fichier ".$config['blacklist_recipient_file']);
	file_put_contents($config['blacklist_recipient_file'], $file_recipient_data);
	// Compilation
	exec($config['path_postmap']." ".$config['blacklist_recipient_file'], $output, $retval);
	if ($retval != 0) {
		echo "CRITICAL ERROR dans la commande postmap\n";
		echo "Fichier blacklist utilisé : ".$config['blacklist_recipient_file'];
		exit(255);
	}
	// Ecriture
	echococo("Ecriture du fichier ".$config['blacklist_sender_file']);
	file_put_contents($config['blacklist_sender_file'], $file_sender_data);
	// Compilation
	exec($config['path_postmap']." ".$config['blacklist_sender_file'], $output, $retval);
	if ($retval != 0) {
		echo "CRITICAL ERROR dans la commande postmap\n";
		echo "Fichier blacklist utilisé : ".$config['blacklist_sender_file'];
		exit(255);
	}
}

// Pour enregistrer la config transport reçu :
function put_transport_file($datas) {
	global $config;
	//var_dump($datas);
	$file_transport_data = '';
	$file_transport_regex_data = '';
	$file_transport_data .= "# OMAILGW-CLI - DON'T TOUCH THIS DYNAMIC FILE ! \n";
	$file_transport_data .= "# INCLUDE SIMILAIRE SAME NAME FILE WITH .static.header EXTENTION \n";	
	$file_transport_regex_data .= "# OMAILGW-CLI - DON'T TOUCH THIS DYNAMIC FILE ! \n";
	$file_transport_regex_data .= "# INCLUDE SIMILAIRE SAME NAME FILE WITH .static.header EXTENTION \n";
	// Vérification de la présence du fichier "static"
	if (is_file($config['transport_file'].".static.header")) {
		echococo("Un fichier ".$config['transport_file'].".static.header est détecté, il sera inclus");
		$file_transport_data .= file_get_contents($config['transport_file'].".static.header")."\n";
	}
	if (is_file($config['transport_regex_file'].".static.header")) {
		echococo("Un fichier ".$config['transport_regex_file'].".static.header est détecté, il sera inclus");
		$file_transport_regex_data .= file_get_contents($config['transport_regex_file'].".static.header")."\n";
	}
	$file_transport_data .= "# / INCLUDE \n";
	$file_transport_regex_data .= "# / INCLUDE \n";

	// Construction du fichier blacklist_file
	foreach ($datas as $data) {
		if ($data['search_regex'] == 1) {
			$file_transport_regex_data .= $data['search']."		".$data['transport'].":".$data['nexthop'];
			$file_transport_regex_data .= "\n";
		} else {
			$file_transport_data .= $data['search']."		".$data['transport'].":".$data['nexthop'];
			$file_transport_data .= "\n";
		}
	}

	$file_transport_data .= "# INCLUDE SAME NAME FILE WITH .static.footer EXTENTION \n";
	$file_transport_regex_data .= "# INCLUDE SIMILAIRE SAME NAME FILE WITH .static.footer EXTENTION \n";
	// Vérification de la présence du fichier "static"
	if (is_file($config['transport_file'].".static.footer")) {
		echococo("Un fichier ".$config['transport_file'].".static.footer est détecté, il sera inclus");
		$file_transport_data .= file_get_contents($config['transport_file'].".static.footer")."\n";
	}
	if (is_file($config['transport_regex_file'].".static.footer")) {
		echococo("Un fichier ".$config['transport_regex_file'].".static.footer est détecté, il sera inclus");
		$file_transport_regex_data .= file_get_contents($config['transport_regex_file'].".static.footer")."\n";
	}
	$file_transport_data .= "# / INCLUDE \n";
	$file_transport_regex_data .= "# / INCLUDE \n";
	// Appel du script custom s'il existe
	if (is_file('custom-transport.php')) {
		echococo("Include custom-transport.php");
		include('custom-transport.php');
	}
	// Ecriture
	echococo("Ecriture du fichier ".$config['transport_file']);
	file_put_contents($config['transport_file'], $file_transport_data);
	// Compilation
	exec($config['path_postmap']." ".$config['transport_file'], $output, $retval);
	if ($retval != 0) {
		echo "CRITICAL ERROR dans la commande postmap\n";
		echo "Fichier transport utilisé : ".$config['transport_file'];
		exit(255);
	}
	// Ecriture
	echococo("Ecriture du fichier ".$config['transport_regex_file']);
	file_put_contents($config['transport_regex_file'], $file_transport_regex_data);
	// Reload
	exec($config['sudo_postfix_reload'], $output, $retval);
	if ($retval != 0) {
		echo "CRITICAL ERROR dans la commande ".$config['sudo_postfix_reload']."\n";
		echo $output;
		echo "\n";
		exit(255);
	}
	
}

// Pour enregistrer la config smtp-auth reçu :
function put_smtp_auth_file($datas) {
	global $config;
	//var_dump($datas);
	$file_data = '';
	// Vérification de la présence du fichier "static"
	if (is_file($config['smtpd_auth_passwd_file'].".static")) {
		echococo("Un fichier ".$config['smtpd_auth_passwd_file'].".static est détecté, il sera inclus");
		$file_data .= "# OMAILGW-CLI - DON'T TOUCH THIS DYNAMIC FILE ! \n";
		$file_data .= "# CREATE SIMILAIRE SAME NAME FILE WITH .static EXTENTION \n";
		$file_data .= file_get_contents($config['smtpd_auth_passwd_file'].".static")."\n";
		$file_data .= "# / OMAILGW-CLI \n";
	}
	// Construction du fichier smtpd_auth_passwd_file
	foreach ($datas as $data) {
		$file_data .= $data['username'] . ":" . cryptPassword($data['password']);
		$file_data .= "\n";
	}
	if (is_file('custom-smtp_auth.php')) {
		echococo("Include custom-smtp_auth.php");
		include('custom-smtp_auth.php');
	}
	// Ecriture
	echococo("Ecriture du fichier ".$config['smtpd_auth_passwd_file']);
	file_put_contents($config['smtpd_auth_passwd_file'], $file_data);
	// Test
	if ($config['smtpd_auth_tester'] != null && filesize($config['smtpd_auth_passwd_file']) != 0) {
		echococo("Test d'authentification : ".$config['smtpd_auth_tester']);
		$variable = array("<username>", "<password>");
		$variable_remplace   = array($data['username'], $data['password']);
		$tester_cmd = str_replace($variable, $variable_remplace, $config['smtpd_auth_tester']);
		exec($tester_cmd." &>/dev/null", $output, $retval);
		if ($retval != 0) {
			echo "CRITICAL ERROR dans le test d'authentification smtp\n";
			echo "CMD utilisé : ".$tester_cmd."\n";
			echo "Fichier smtpd_auth utilisé : ".$config['smtpd_auth_passwd_file']."\n";
			exit(255);
		}
	}
}

function get_rspamd_data(){
	echococo("get_rspamd_data");
	global $config;
	try {
		$curl = curl_init();
		$curlopt = [
			CURLOPT_URL => $config['rspamd_url'],
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [	"User-Agent: oMailgw-cli (https://framagit.org/omailgw/omailgw-cli)" ],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 15,
			CURLOPT_CONNECTTIMEOUT => 15,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		];
		curl_setopt_array($curl, $curlopt);
		$result = curl_exec($curl);
		$curl_error = curl_error($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($http_code != intval(200)) {
			echococo("Code HTTP : " . $http_code . " Erreur : " . json_encode($response));
			echococo("Reponse : " . json_encode($result));
			echococo("curl_error : " . $curl_error);
			exit(9);
		} else {
			file_put_contents($config['rspamd_cache'], $result);
		}
	} catch (\Throwable $th) {
		throw $th;
	} finally {
		curl_close($curl);
	}
}

function parse_rspamd_data($queueId, $datas){
	echococo("parse_rspamd_data");
	global $config;
	// Recherche du message_id par queueId
	$message_id = null;
	foreach($datas as $data) {
		if (isset($data['queueId']) && $data['queueId'] == $queueId && isset($data['message_id'])) {
			$message_id = $data['message_id'];
			echococo("message ID trouvé : " . $message_id);
			continue;
		}
	}
	// Si le queueId n'a pas été trouvé, on abandonne
	if($message_id === null) {
		echococo("message ID inconnu avec la queueId : " . $queueId);
		return false;
	}
	// On parse le cache de rspamd
	$data="";
	$rspamd_historys=json_decode(file_get_contents($config['rspamd_cache']), true);
	foreach($rspamd_historys['rows'] as $rspamd_history) {
		if ($rspamd_history['action'] == 'reject' && $rspamd_history['message-id'] == $message_id) {
			$data.="RSPAMD_SCORE:".$rspamd_history['score'];
			echococo("RSPAMD : La rows par message_id + action reject est trouvé");
			foreach($rspamd_history['symbols'] as $rspamd_history_symbole_key=>$rspamd_history_symbole) {
				if ($rspamd_history_symbole['score'] > 0) {
					$data.="\n".$rspamd_history_symbole['name'].":".$rspamd_history_symbole['score'];
				}
			}
		}
	}
	echococo("rspamd score : \n".$data);
	return $data;
}
